# hsv2rgb

This golang package provides a conversion function from HSV to RGB

# usage

```golang
import gitlab.com/mpizka/hsv2rgb

func main() {
    hue, saturation, value := 125, 1.0, 1.0                     // HSV as float64 values
    red, green, blue := hsv2rgb.HSV2RGB(hue, saturation, value) // RGB as uint8 values
}
```