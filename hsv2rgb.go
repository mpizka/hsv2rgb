// hsv2rgb provides a conversion function from HSV to RGB color
//
// HSV is useful in many applications relating to how humans
// perceive color as a spectrum (e.g. heatmaps). RGB however caters
// to how machines display colors.
//
// credit for the formula goes to the authors of the german Wikipedia:
// https://de.wikipedia.org/wiki/HSV-Farbraum#Umrechnung_HSV_in_RGB
package hsv2rgb

import "math"

// HSV2RGB converts an HSV color (hue[0, 360] saturation[0,1] value[0,1]) to an
// RGB 3-byte tuple. No validation is done, if your values are outside
// the HSV definition, you are on your own.
func HSV2RGB(H, S, V float64) (R, G, B uint8) {
    Hi := math.Floor(H / 60)    // base color interval (BCI)
    f  := (H / 60) - Hi         // value within BCI [0,1]
    p  := V * (1 - S)           // lengths
    q  := V * (1 - S * f)
    t  := V * (1 - S * (1 -f))

    var r, g, b float64
    switch {
        case Hi == 1:
            r, g, b = q, V, p
        case Hi == 2:
            r, g, b = p, V, t
        case Hi == 3:
            r, g, b = p, q, V
        case Hi == 4:
            r, g, b = t, p, V
        case Hi == 5:
            r, g, b = V, p, q
        default:
            r, g, b = V, t, p   // Hi elementof {0, 6}
    }

    R = uint8(math.Round(r * 255))
    G = uint8(math.Round(g * 255))
    B = uint8(math.Round(b * 255))

    return
}
